import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {FaGitlab} from 'react-icons/fa'


const HeaderSocials = () => {
    return (
        <div className='header__socials'>
            <a href="https://linkedin.com" target="_blank"><BsLinkedin/></a>
            <a href="https://gitlab.com" target="_blank"><FaGitlab/></a>
        </div>
    )
}

export default HeaderSocials