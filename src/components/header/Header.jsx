import React from 'react'
import './header.css'
import  CTA from './CTA'
import ME from '../../assets/my pic.jpg'
import HeaderSocials from './HeaderSocials'

const Header = () => {
  return (
    <header>
      <section id='header' className="container header__container">
        <h5>Hello I'm</h5>
        <h1>Hamed Karami</h1>
        <h5 className="text-light">Android Developer</h5>
        <CTA />
        <HeaderSocials />

        <div className='me'>
          <img src={ME} alt="me" className='me-image' />
        </div>

        <a href="#contact" className='scroll__down'>Scroll Down</a>
      </section>
    </header>
  )
}

export default Header