import React from 'react'
import './testimonials.css'
import AVA1 from '../../assets/ava1.jpg'
import AVA2 from '../../assets/ava2.jpg'
import AVA3 from '../../assets/ava3.jpg'

import {Pagination } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';


const Testimonials = () => {
  return (
    <section id='testomonials'>
      <h5>Review from clients</h5>
      <h2>Testomonials</h2>
      <Swiper className="container testimonials__container"
            modules={[Pagination]}
            spaceBetween={40}
            slidesPerView={1}
            pagination={{ clickable: true }}
            >
        <SwiperSlide className="testimonial">
          <div className="client__avatar">
            <img src={AVA1} alt='Avatar One' />
          </div>
          <h5 className='client__name'>Hamed Karami</h5>
          <small className='client__review'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </small>
        </SwiperSlide>

        <SwiperSlide className="testimonial">
          <div className="client__avatar">
            <img src={AVA2} alt='Avatar Two' />
          </div>
          <h5 className='client__name'>Hamed Karami</h5>
          <small className='client__review'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </small>

        </SwiperSlide>

        <SwiperSlide className="testimonial">
          <div className="client__avatar">
            <img src={AVA3} alt='Avatar Three' />
          </div>
          <h5 className='client__name'>Hamed Karami</h5>
          <small className='client__review'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
          </small>
        </SwiperSlide>
      </Swiper>
    </section>
  )
}

export default Testimonials