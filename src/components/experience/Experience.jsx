import React from 'react'
import './experience.css'
import { BsPatchCheckFill } from 'react-icons/bs'

const Experience = () => {
  return (
    <section id='experience'>
      <h5>What Skills I Have</h5>
      <h2>My Experience</h2>

      <div className="container experience__container">
        <div className='expeience__frontend'>
          <div className="experience__content">

          <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
                <h4>ANDROID</h4>
                <small className='text-light'>Experienced</small>
              </div>
            </article>

          <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
                <h4>JAVA</h4>
                <small className='text-light'>Experienced</small>
              </div>
            </article>

            
            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
                <h4>KOTLIN</h4>
                <small className='text-light'>Experienced</small>
              </div>
            </article>

            
            <article className='experience__details'>
              <BsPatchCheckFill className='experience__details-icon'/>
              <div>
                <h4>C#</h4>
                <small className='text-light'>Experienced</small>
              </div>

            </article>

          </div>
        </div>

        {/* End of frontEnd */}

      </div>
    </section>
  )
}

export default Experience