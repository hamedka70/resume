import React from 'react'

import './portfolio.css'
import IMG1 from '../../assets/p1.webp'
import IMG2 from '../../assets/p2.jpg'
import IMG3 from '../../assets/p3.jpg'

const Portfolio = () => {
  return (
    <section id='portfolio'>
      <h5>My Recent Work</h5>
      <h2>Portfolio</h2>
      <div className='container portfolio__container'>
        <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG1} alt="" />

          </div>
          <h3>This is aportfolio item title</h3>
          <div className="portfolio__item-cta">
            <a href='https://gitlab.com/hamedka70' className='btn' target='_blank'>Gitlab</a>
            <a href='https://gitlab.com/hamedka70' className='btn btn-primary' target='_blank'>Live Demo</a>
          </div>

        </article>

        <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG2} alt="" />

          </div>
          <h3>This is aportfolio item title</h3>
          <div className="portfolio__item-cta">
            <a href='https://gitlab.com/hamedka70' className='btn' target='_blank'>Gitlab</a>
            <a href='https://gitlab.com/hamedka70' className='btn btn-primary' target='_blank'>Live Demo</a>
          </div>
        </article>

        <article className='portfolio__item'>
          <div className="portfolio__item-image">
            <img src={IMG3} alt="" />

          </div>
          <h3>This is aportfolio item title</h3>
          <div className="portfolio__item-cta">
            <a href='https://gitlab.com/hamedka70' className='btn' target='_blank'>Gitlab</a>
            <a href='https://gitlab.com/hamedka70' className='btn btn-primary' target='_blank'>Live Demo</a>
          </div>
        </article>
      </div>
    </section>
  )
}

export default Portfolio